# Generated by Django 3.1.6 on 2021-09-19 20:26

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('preinscripciones', '0002_auto_20210919_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='grupo',
            name='cupo_limite',
            field=models.IntegerField(default=40, validators=[django.core.validators.MinValueValidator(1, django.core.validators.MaxValueValidator(50))], verbose_name='Cupo limite'),
        ),
        migrations.AlterField(
            model_name='grupo',
            name='cupo',
            field=models.IntegerField(),
        ),
    ]
