from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.

#Modelo referencial para el grupo
class Grupo(models.Model):
    letra = models.CharField(max_length = 1)
    cupo_limite=models.IntegerField('Cupo limite', validators=[MinValueValidator(1,MaxValueValidator(50))], default=40)
    cupo = models.IntegerField(blank=True, null=True)
    semestre= models.ForeignKey("preinscripciones.Semestre", verbose_name="Semestre", on_delete=models.CASCADE)
    horario=models.FileField(upload_to="", default=None, blank=True)
    def __str__(self):
        return self.letra

    def set_cupos_restantes(self,cupos_usados=0):
        self.cupo=self.cupo_limite - cupos_usados
        self.save()

class Grupo_Aspirante(models.Model):
    class Meta:
        unique_together=(("grupo","aspirante"),)
    grupo=models.ForeignKey("preinscripciones.Grupo", verbose_name="Grupo", on_delete=models.CASCADE)
    aspirante=models.ForeignKey("usuarios.Aspirante", verbose_name="Aspirante", on_delete=models.CASCADE)

#Modelo referencial para la materia
class Materia(models.Model):
    nombre = models.CharField(max_length=60)
    descripcion = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre

#Modelo referencial para el semestre
class Semestre(models.Model):
    numero=models.IntegerField('Semestre')
    def __str__(self):
        return str(self.numero)

#Modelo referencial para la relación de una materia con un grupo. Diseño para relaciones muchos a muchos.
class Grupo_materia(models.Model):
    grupo=models.ForeignKey("preinscripciones.Grupo", verbose_name="Grupo", on_delete=models.CASCADE)
    materia=models.ForeignKey("preinscripciones.Materia", verbose_name="Materia", on_delete=models.CASCADE)

#Modelo referencial para la relación de un docente con una materia. Diseño para relaciones muchos a muchos.
class Materia_docente(models.Model):
    class Meta:
        unique_together=(('materia','docente'),)
    materia = models.ForeignKey("preinscripciones.Materia", verbose_name = 'Materia', on_delete = models.CASCADE)
    docente = models.ForeignKey("usuarios.Docente", verbose_name = 'Docente', on_delete = models.CASCADE)

    def __str__(self) -> str:
        return self.materia.nombre+" - "+self.docente.last_name+" "+self.docente.first_name

#Modelo referencial para la relación de un docente que imparte una materia en un grupo específico. Diseño para relaciones muchos a muchos.
class Grupo_materia_docente(models.Model):
    class Meta:
        unique_together=(('materia_docente','grupo'),)
    materia_docente=models.ForeignKey("preinscripciones.Materia_docente", verbose_name="materia_docente", on_delete=models.CASCADE)
    grupo=models.ForeignKey("preinscripciones.Grupo",verbose_name="Grupo", on_delete=models.CASCADE)