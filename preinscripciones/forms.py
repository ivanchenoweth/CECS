from django import forms
from .models import Grupo, Materia, Grupo_materia_docente, Materia_docente

#Formulario para la creacion y edicion de grupos.
class GrupoForm(forms.ModelForm):
    class Meta:
        model=Grupo
        fields="letra", "cupo_limite","semestre","horario"

        widgets={
            'letra':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Grupo'}),
            'cupo_limite':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Cupo máximo'}),
            'semestre':forms.Select(attrs={'class':'form-control', 'placeholder':'Semestre'})
        }

#Formulario para la creacion y edicion de materias.
class MateriaForm(forms.ModelForm):
    class Meta:
        model=Materia
        fields="__all__"

        widgets={
            "nombre":forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre'}),
            "descripcion":forms.TextInput(attrs={'class':'form-control', 'placeholder':'Descripcion'})
        }

#Formulario para la creacion y edicion de la relación de una materia con un docente (maestro).
class MateriaDocenteForm(forms.ModelForm):
    class Meta:
        model=Materia_docente
        fields="__all__"
        widgets={
            'materia':forms.Select(attrs={'class':'form-control','placeholder':"Materia"}),
            "docente":forms.Select(attrs={'class':'form-control', 'placeholder':'Docente'})
        }

#Formulario para la creacion y edicion de la relacion de un docente con una materia en un grupo en específico.
class GrupoMateriaDocenteForm(forms.ModelForm):
    class Meta:
        model=Grupo_materia_docente
        fields="__all__"
        widgets={
            "materia_docente":forms.Select(attrs={'class':'form-control', 'placeholder':'Materia'}),
            "grupo":forms.Select(attrs={'class':'form-control', 'placeholder':'Grupo'})
        }