from .models import Aspirante, Docente
from django.contrib import admin

# Register your models here.

admin.site.register(Aspirante)
admin.site.register(Docente)