from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django import forms
from django.contrib.auth.models import User
from django.forms import widgets

# Clase para manejar al aspirante.
## Modelo que extiende de la base de django User.
class Aspirante(User):
    num_aspirante=models.IntegerField()
    comprobante_pago=models.FileField(upload_to="", blank=False)
    status=models.BooleanField(default=0)
    grupo = models.ForeignKey("preinscripciones.Grupo", verbose_name = 'Grupo', on_delete = models.CASCADE, default=None, null=True, blank=True)
    correo=models.EmailField()
    def __str__(self):
        return self.num_aspirante

    def get_grupo(self):
        return self.grupo
# Clase para manejar al docente.
## Modelo que extiende de la base de django User.

class Docente(User):
    matricula = models.CharField(max_length=8)
    telefono = models.BigIntegerField(blank=True, null=True)
    correo=models.EmailField()
    def __str__(self):
        return str(self.last_name)+" "+str(self.first_name)