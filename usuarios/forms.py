from django import forms
from .models import Aspirante, Docente

# Fomrulario para manejar los aspirantes de la unidad academica, su creacion dentro del sistema..
class AspiranteForm(forms.ModelForm):
    class Meta:
        model = Aspirante

        fields = 'username','first_name', 'last_name', 'email', 'password', 'num_aspirante', 'comprobante_pago'
        help_texts = {
            'username': None,
        }
        widgets = {
                'username':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}),
                'first_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre(s)'}),
                'last_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellidos'}),
                'email':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email'}),
                'password':forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Contraseña'}),
                'num_aspirante':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Numero de aspirante'}),
                'comprobante_pago':forms.FileInput(attrs={'class':'form-control', 'placeholder':'Comprobante de pago'}),
        }

    # Funcion que permite guardar al usuario del aspirante.
    def save(self, commit=True):
        user = super(AspiranteForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password']) #Encripta la contraseña
        if commit:
            user.save()
        return user
    
# Formulario para modificar los datos de un aspirante ya registrado en el sistema.
class EditarAspiranteForm(forms.ModelForm):
    class Meta:
        model = Aspirante

        fields = 'username','first_name', 'last_name', 'correo',  'comprobante_pago'

        widgets = {
                'username':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}),
                'first_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre(s)'}),
                'last_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellidos'}),
                'correo':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Correo'}),
                'comprobante_pago':forms.FileInput(attrs={'class':'form-control', 'placeholder':'Comprobante de pago'}),
        }

# Fomrulario que permite la creación de docentes en el sistema.
class DocenteForm(forms.ModelForm):
    class Meta:
        model = Docente

        fields = 'username', 'first_name', 'last_name', 'correo', 'password', 'matricula', 'telefono'
        help_texts = {
            'username': None,
        }
        widgets = {
                'username':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}),
                'first_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre(s)'}),
                'last_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellidos'}),
                'correo':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Correo'}),
                'telefono':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Numero de Telefono'}),
                'password':forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Contraseña'}),
                'matricula':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Matricula del docente'}),
        }

    def save(self, commit=True):
        user = super(DocenteForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password']) #Encripta la contraseña
        if commit:
            user.save()
        return user

class DocenteUpdateForm(forms.ModelForm):
    class Meta:
        model = Docente

        fields = 'first_name', 'last_name', 'correo', 'matricula', 'telefono'

        widgets = {
                'first_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Nombre(s)'}),
                'last_name':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Apellidos'}),
                'correo':forms.TextInput(attrs={'class':'form-control', 'placeholder':'Correo'}),
                'telefono':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Numero de Telefono'}),
                'matricula':forms.NumberInput(attrs={'class':'form-control', 'placeholder':'Matricula del docente'}),
        }